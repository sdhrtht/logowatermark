#!/bin/bash
dir=`pwd`
fileDir=$(date +%Y%m%d)
mkdir -p "Videos//Clean//${fileDir}"
mkdir -p "Videos//Watermarked//${fileDir}"
for file in `find "${dir}/Videos/ToBeProcessed/" -type f -name "*.mp4"`;
do
  echo "$file"
  if [ -f "$file" ]; then
    extn="${file##*\.}"
    echo "${extn}"
    val1=$(date +%s%3N)
    mv "$file" "${val1}.${extn}"
    val2=$(date +%s%3N)
    ffmpeg -i "${val1}.${extn}" -vf "scale=1280:720:force_original_aspect_ratio=increase,crop=1280:720" "${val2}.${extn}"
    val3=$(date +%s%3N)
    ffmpeg -i "${val2}.${extn}" -i BGAudioLibrary/Birds-chirping-sound-effect.mp3 -map 0:0 -map 1:0 -c:v copy -c:a libmp3lame -q:a 1 -shortest "${val3}.${extn}"
    val4=$(date +%s%3N)
    ffmpeg -i "${val3}.${extn}" -i logo.png -filter_complex "overlay=5:H-h-5:format=auto,format=yuv420p" -c:a copy  "${val4}.${extn}"
    mv ${val1}.${extn} Videos/Clean/"$fileDir"
    mv ${val4}.${extn} Videos/Watermarked/"$fileDir"
    rm ${val2}.${extn} ${val3}.${extn}
  fi
done