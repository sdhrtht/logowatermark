
# Hey! Logo Watemarker 👋

![visitors](https://visitor-badge.laobi.icu/badge?page_id=sdhrtht.logowatermark)

# How to Run

## Prerequites:

1) Linux PC
2) ffmpeg installed

## Steps for Encoding Watermark:

1) Clone or Download the Source
2) copy the clean file to "ToBeProcessed" Directory
    * Images to `Images/ToBeProcessed `
    * Videos to `Videos/ToBeProcessed`
3) Replace Logo with your tranparent Logo
4) make script update if needed to change background audio
    * Sample Audio are available in the `BGAudioLibrary`
    * Audio files are downloaded from [Free Sounds Library](https://www.freesoundslibrary.com/animal-sounds/)

---

# Known Issues or Improvements:
1) scripts need to be updated to handle file names with spaces.
2) Need to convert to poweshell or Batch scripting to work on windows.
3) python would be a good choice to make it work on both platforms (future update)

---

# Addtional Useful Commands 

These scripts uses most of the commands.

---

## find the resolutions:

C:\ffmpeg\bin\ffprobe.exe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 input.mp4

## Resize to video with out loosing Resolution.

`C:\ffmpeg\bin\ffmpeg.exe -i input.mp4 -vf "scale=1280:720:force_original_aspect_ratio=increase,crop=1280:720" output.mp4`

## Replace Audio.

`C:\ffmpeg\bin\ffmpeg.exe -i output.mp4 -i Birds-chirping-sound-effect.mp3 -map 0:0 -map 1:0 -c:v copy -c:a libmp3lame -q:a 1 -shortest output_with_newAudio.mp4`

## Add Watermark File.

`C:\ffmpeg\bin\ffmpeg.exe -i outputWithNewAudio.mp4 -i 1.jpeg -filter_complex  "[1][0]scale2ref=w=oh*mdar:h=ih*0.1[logo][video];[video][logo]overlay=W-w-5:5" -c:a copy  outputWithNewAudioAndWatermark.mp4`

## for split Video to 15sec intervals

`ffmpeg -i fff.avi -acodec copy -f segment -segment_time 15 -vcodec copy -reset_timestamps 1 -map 0 fff%d.avi`

*Use -an after -map 0 if you don't want the resulting clips to have sound.*

## for split video with selected time slots:

`ffmpeg -i input.avi -vcodec copy -acodec copy -ss 00:00:00 -t 00:30:00 output1.avi`
`ffmpeg -i input.avi -vcodec copy -acodec copy -ss 00:30:00 -t 00:30:00 output2.avi`

*to do same in single command*

`ffmpeg -i input.avi -vcodec copy -acodec copy -ss 00:00:00 -t 00:30:00 output1.avi -vcodec copy -acodec copy -ss 00:30:00 -t 00:30:00 output2.avi`


## For Image

`C:\ffmpeg\bin\ffmpeg.exe -i 'WhatsApp Image 2021-08-19 at 2.06.43 AM.jpeg' -vf scale="'if(gt(a,1/1),1280,-1)':'if(gt(a,1/1),-1,960)'" OUT.jpg`

## Addtional Commands for reference

`C:\ffmpeg\bin\ffmpeg.exe -i budgies.mp4 -c:v copy -an .\budgiesWithoutAudio.mp4
C:\ffmpeg\bin\ffmpeg.exe -i budgies.mp4 -i Birds-chirping-sound-effect.mp3 -c copy -map 0:v:0 -map 1:a:0 .\budgiesWithAudio.mp4`
`C:\ffmpeg\bin\ffmpeg.exe -i 'WhatsApp Image 2021-08-19 at 2.06.43 AM.jpeg' -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2" OUT.jpg`

## remove white spaces in files name in bash:
``for f in *; do mv "$f" `echo $f | tr ' ' '_'`; done``

`C:\ffmpeg\bin\ffmpeg.exe -i 'WhatsApp Video 2021-08-19 at 9.36.24 AM.mp4' -vf "scale=1280:720:force_original_aspect_ratio=increase,crop=1280:720" output.mp4`

`C:\ffmpeg\bin\ffmpeg.exe -i .\output.mp4 -i Birds-chirping-sound-effect.mp3 -map 0:0 -map 1:0 -c:v copy -c:a libmp3lame -q:a 1 -shortest output_with_newAudio.mp4`

`C:\ffmpeg\bin\ffmpeg.exe -i .\output_with_newAudio.mp4 -i .\logo2.png -filter_complex "overlay=5:H-h-5:format=auto,format=yuv420p" -c:a copy  budgies2.mp4`

# References:

[How to Add a Watermark to Video](https://gist.github.com/bennylope/d5d6029fb63648582fed2367ae23cfd6)
[How to add transparent watermark in center of a video with ffmpeg?](https://stackoverflow.com/questions/10918907/how-to-add-transparent-watermark-in-center-of-a-video-with-ffmpeg)
