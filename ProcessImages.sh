#!/bin/bash
dir=`pwd`
fileDir=$(date +%Y%m%d)
mkdir -p "Images//Clean//${fileDir}"
mkdir -p "Images//Watermarked//${fileDir}"
for file in `find "${dir}/Images/ToBeProcessed/" -type f -name "*.jpeg" -o -name "*.jpg"`;
do
  if [ -f "$file" ]; then
    extn="${file##*\.}"
    echo "${extn}"
    val1=$(date +%s%3N)
    mv "$file" "${val1}.${extn}"
    val2=$(date +%s%3N)
    ffmpeg -i "${val1}.${extn}" -vf scale="'if(gt(a,1/1),1280,-1)':'if(gt(a,1/1),-1,960)'" "${val2}.${extn}"
    val3=$(date +%s%3N)
    ffmpeg -i "${val2}.${extn}" -i logo.png -filter_complex "overlay=5:H-h-5:format=auto,format=yuv420p" -c:a copy  "${val3}.${extn}"
    mv ${val1}.${extn} Images/Clean/"$fileDir"
    mv ${val3}.${extn} Images/Watermarked/"$fileDir"
    rm ${val2}.${extn}
  fi
done